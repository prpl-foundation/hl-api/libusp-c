/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <unistd.h>
#include <stdint.h>
#include <string.h>

#include "mtp_mqtt.h"

// WARNING: this file contains non-functional example code. Some big shortcuts were taken to give a
// quick overview of how this could work

/* Use the generated functions to pack the protobuf message and publish it */
static int mqtt_send(usp_ctx_t* ctx, const char* to_id, Usp__Msg* msg, int* msg_id) {
    void* binary_protobuf = pack_usp_message(msg, to_id, msg_id);
    char* topic = mqtt_topic_from_mqtt_properties();

    mqtt_publish(binary_protobuf, topic);
}

static int mqtt_recv(usp_ctx_t* ctx, char** from_id, Usp__Msg** msg, int* msg_id) {
    int fd = usp_get_fd(ctx);
    UspRecord__Record* rec = NULL;
    uint8_t* bytes = (uint8_t*) calloc(1, 1024);

    // Note: need to realloc memory and read more if more than 1024 bytes are available
    read(fd, bytes, 1024);

    unpack_binary_record(bytes, &rec);
    *from_id = strdup(rec->from_id);

    unpack_binary_message(rec, msg);
    *msg_id = strdup((*msg)->header->msg_id);
}

int mtp_mqtt_init(usp_ctx_t* ctx) {
    // Get the private data that was passed to usp_new:
    mqtt_data_t* data = (mqtt_data_t*) ctx->priv;

    // Fill the endpoint id
    ctx->endpoint_id = data->endpoint_id;

    // Connect to mqtt broker
    mqtt_connect(data->broker_address, data->broker_port);

    // Subscribe to a topic to receive USP messages. Assume this returns a file descriptor that can
    // be read for incoming messages (to keep things simple)
    int fd = mqtt_subscribe(data->recv_topic);

    // Set the file descriptor in the context
    ctx->fd = fd;

    // Set the send and receive functions
    ctx->recv = mqtt_recv;
    ctx->send = mqtt_send;

    return 0;
}

int mtp_mqtt_clean(usp_ctx_t* ctx) {
    // Get the private data that was passed to usp_new:
    mqtt_data_t* data = (mqtt_data_t*) ctx->priv;

    // Unsubscribe from topic
    mqtt_unsubscribe(data->recv_topic);

    // Disconnect from broker
    mqtt_disconnect(data->broker_address, data->broker_port);

    return 0;
}