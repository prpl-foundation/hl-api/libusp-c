/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USP_COMMON_H__)
#define __USP_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <uspprotobuf/usp-msg-1-1.pb-c.h>
#include <uspprotobuf/usp-record-1-1.pb-c.h>

typedef struct _usp_ctx usp_ctx_t;

typedef int (* usp_init_fn) (usp_ctx_t* ctx);
typedef int (* usp_clean_fn) (usp_ctx_t* ctx);

typedef int (* usp_mtp_send_fn) (usp_ctx_t* ctx, const char* to_id, Usp__Msg* msg, int* msg_id);
typedef int (* usp_mtp_recv_fn) (usp_ctx_t* ctx, char** from_id, Usp__Msg** msg, int* msg_id);

/**
 * Enum of valid MTP ids
 *
 * The MTP id can be used by the init function to select the correct mtp
 * send and receive functions.
 *
 */
typedef enum {
    USP_MTP_COAP,
    USP_MTP_STOMP,
    USP_MTP_WEBSOCKET,
    USP_MTP_MQTT5,
    USP_MTP_IMTP
} mtp_id_t;

/**
 * A USP context structure
 *
 * Each USP Agent or USP controller can contain multiple of these structures
 *
 * Typically such a structure is created for each MTP that is used or supported
 * by the controller or agent
 *
 * This context structure contains some callback functions
 *
 * The init callback function is called when the USP context is created or
 * initialized using @ref usp_new or @ref usp_init. The init function
 * must set-up the MTP and set the correct send and receive functions in
 * the USP context structure. If some information is needed to set-up the MTP
 * it can be passed in the private data. The private data is opaque for this
 * implementation and can contain a pointer to any data, including information
 * to set-up the MTP.
 *
 * The clean callback function is called when the USP context is deleted or
 * cleaned using @ref usp_delete or @ref usp_clean. The clean function
 * must teardown the MTP.
 *
 * The send function, which is set by the init function, is called whenever
 * a USP message needs to be sent using this USP context
 *
 * The recv function, which is set by the init function, can be called to read
 * a USP message.
 *
 * Both send and recv function MUST not block (non-blocking I/O).
 *
 * The fd (filedescriptor) can be filled, or set to -1.
 * The fd can be used to be monitored for data in an event loop (typically using
 * select, poll, or any other mechanism). When data is available for reading,
 * the event loop can then call @ref usp_read using the usp_ctx as argument
 *
 */
typedef struct _usp_ctx {
    usp_init_fn init;
    usp_clean_fn clean;

    usp_mtp_send_fn send;
    usp_mtp_recv_fn recv;

    int timeout; // in seconds
    mtp_id_t mtp_id;

    int fd;
    const char* endpoint_id;
    void* priv;
} usp_ctx_t;

/**
 * Allocates a usp context structure
 *
 * Fills the init and clean functions, fills the private data
 *
 * Calls the init function which must:
 * - fill the endpoint id (mandatory)
 * - fill the file descriptor (optional)
 * - set the send function (mandatory - can be mtp dependent)
 * - set the recv function (mandatory - can be mtp dependent)
 * - do all set-up (for mtp if needed)
 *
 * The priv data is optional and can be NULL.
 *
 */
int usp_new(usp_ctx_t** usp_ctx, mtp_id_t mtp, usp_init_fn init, usp_clean_fn clean, void* priv);

/**
 * Calls the clean function which must
 *  - do all clean-up and teardown
 *
 * Frees the memory that was allocated for the usp context and sets the pointer
 * to NULL
 *
 */
int usp_delete(usp_ctx_t** usp_ctx);

/**
 * Initialize the provided usp context struct
 *
 * Fills the init and clean functions, fills the private data
 *
 * Calls the init function which must:
 * - fill the endpoint id (mandatory)
 * - fill the file descriptor (optional)
 * - set the send function (mandatory - can be mtp dependent)
 * - set the recv function (mandatory - can be mtp dependent)
 * - do all set-up (for mtp if needed)
 *
 * The priv data is optional and can be NULL.
 *
 */
int usp_init(usp_ctx_t* usp_ctx, mtp_id_t mtp, usp_init_fn init, usp_clean_fn clean, void* priv);

/**
 * Clean-up the usp context structure
 *
 * Calls the clean function which must
 *  - do all clean-up and teardown
 *
 */
int usp_clean(usp_ctx_t* usp_ctx);

/**
 * Set the timeout for synchronous requests
 *
 */
int usp_set_timeout(usp_ctx_t* usp_ctx, int timeout);

/**
 * Get the file descriptor if any
 *
 * This is mainly used to make it possible to add it to an eventloop.
 *
 */
int usp_get_fd(usp_ctx_t* usp_ctx);

/**
 * Reads the next msg
 *
 * When a msg is read, memory will be allocated for the msg and the
 * msg argument is filled in. The from_id argument will be filled up with the
 * id of the sender. This must be freed using free.
 *
 */
int usp_read(usp_ctx_t* usp_ctx, Usp__Msg** msg, char** from_id);

/**
 * Send a response
 *
 * The caller must build a Usp__Response struct and provide the pointer to that struct
 *
 * The provided msg_id must match the message id from the received request
 *
 */
int usp_send_response(const usp_ctx_t* usp_ctx, const char* to_id, uint32_t msg_id, const Usp__Response* response);

/**
 * Send an error
 *
 * The caller must build a Usp__Error struct and provide the pointer to that struct
 *
 * The provided msg_id must match the message id from the received request
 *
 */
int usp_send_error(const usp_ctx_t* usp_ctx, const char* to_id, uint32_t msg_id, const Usp__Error* error);

#ifdef __cplusplus
}
#endif

#endif // __USP_COMMON_H__
