/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdint.h>

#include "evloop_example.h"
#include "mtp_mqtt.h"

#define UNUSED __attribute__((unused))

static usp_ctx_t* mqtt_ctx = NULL;
static struct event* mqtt_ev = NULL;

/* Protobuf memory management helper struct */
static ProtobufCAllocator protobuf_allocator =
{
    protobuf_alloc,
    protobuf_free,
    NULL   // Opaque pointer passed to above 2 functions. Currently unused by those functions.
};

/* Protobuf memory management helper function */
static void* protobuf_alloc(UNUSED void* allocator_data, size_t size) {
    void* ptr;

    ptr = malloc(size);

    return ptr;
}

/* Protobuf memory management helper function */
static void protobuf_free(UNUSED void* allocator_data, void* pointer) {
    free(pointer);
}

/**
 * The functions called in the switch are not defined anywhere in this example. If they were
 * implemented, they would handle the incoming message and then call usp_send_response or
 * usp_send_error depending on the situation.
 */
static void handle_usp_message(usp_ctx_t* ctx, Usp__Msg* msg, const char* to_id) {
    uint32_t msg_type = msg->header->msg_type;

    switch(msg_type) {
    case USP__HEADER__MSG_TYPE__GET:
        handle_get(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__NOTIFY:
        handle_notify(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__SET:
        handle_set(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__OPERATE:
        handle_operate(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__ADD:
        handle_add(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__DELETE:
        handle_delete(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM:
        handle_get_supported_dm(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__GET_INSTANCES:
        handle_get_instances(ctx, msg, to_id);
        break;
    case USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO:
        handle_get_supported_proto(ctx, msg, to_id);
        break;
    default:
        break;
    }
}

/* This function will be called whenever there is data ready to read from the usp_ctx_t file descriptor */
static void usp_read_cb(UNUSED evutil_socket_t fd,
                        UNUSED short flags,
                        void* arg) {
    Usp__Msg* msg = NULL;
    usp_ctx_t* ctx = (usp_ctx_t*) arg;
    char* from_id = NULL;

    // Read the incoming USP message
    usp_read(ctx->fd, &msg, &from_id);

    // Handle USP message and send appropriate response
    handle_usp_message(ctx, msg, from_id);

    // This free function is generated from the .proto files.
    usp__msg__free_unpacked(msg, &protobuf_allocator);
    free(from_id);
}

static void init_all(void) {
    mqtt_data_t data = {
        .endpoint_id = "dummy",
        .broker_address = "example_broker",
        .broker_port = 1883,
        .recv_topic = "foo",
    };

    // Setup event loop
    el_create();

    // Dynamically allocate memory for a new USP context structure that will use the MQTT MTP
    usp_new(&mqtt_ctx, USP_MTP_MQTT5, mtp_mqtt_init, mtp_mqtt_clean, &data);

    // Set an arbitrary timeout value if needed
    usp_set_timeout(mqtt_ctx, 5);

    // Add file descriptor of usp context to event loop. In this pseudo code, the event that was
    // added is returned by the function and tracked in a static variable. In real code, this can be
    // done in a better way.
    mqtt_ev = el_add_fd(usp_get_fd(&mqtt_ctx), usp_read_cb, &mqtt_ctx);
}

static void cleanup_all(void) {
    usp_delete(&mqtt_ctx);
    el_remove_fd(mqtt_ev);
    el_destroy();
}

int main(void) {
    init_all();

    el_start();

    cleanup_all();

    return 0;
}
