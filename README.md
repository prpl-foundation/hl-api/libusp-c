# libusp-c

## Introduction

This library contains an API description for communication between a main USP agent and its managed reduced agents on the gateway, in a container or in the cloud. The messages that will be used for communication are USP protobuf messages as specified by the TR-369 (USP) specification. This library does not dictate which Message Transfer Protocol (MTP) must be used to transmit these messages, but it does limit the possible MTPs to those specified in TR-369 and discussed within prpl:

- CoAP
- STOMP
- Websockets
- MQTT 5.0
- IMTP

The library could also specify a default MTP which will be used if no other MTP is specified. This is useful for parties that are not interested in which underlying MTP is used, but just want to be able to communicate with (reduced) agents.

## API structure

The API consists of 4 header files:

- usp_common.h
- usp_agent.h
- usp_controller.h
- usp.h

The `usp_common.h` file describes the common functionality that is needed by both an agent and a controller. It contains the functions needed to set up the MTPs and to send or receive messages. The `usp_controller.h` file contains all the requests that need to be sent by a USP controller using this API. So there is a function for sending a `get`, `set`, `add`, `delete`, `get_supported_dm`, `get_instances` and `operate` message. On the other hand, the `usp_agent.h` file only contains the a function to send `notify` messages. Sending a response can be done with the `usp_send_response` function, while receiving any message can be done with `usp_read`. Both of these functions are part of the common functionality, so they can be found in `usp_common.h`.

Finally there is the `usp.h` file, which simply includes all 3 files. When you implement an endpoint that acts as both an agent and controller, you will likely need all provided functions and can simply include this single header file.

> Note: An endpoint that provides a part of the data model can be seen as a USP agent, while an endpoint that requests something from an endpoint exposing a data model can be seen as a USP controller.

## What is not yet available in this API

This API does not yet describe how subscriptions should be made from a main USP agent to a reduced agent. This is something that does not exist inside the USP specification, because the concept of a reduced agent does not exist.

When an external USP controller subscribes to notifications from the main USP agent, it adds an entry in the `LocalAgent.Subscription.{i}.` data model. This table in the data model is useful for the controller to know which active subscriptions it has. But what happens when a USP controller adds a subscription to a part of the data model that is managed by a reduced agent? The USP controller will simply add an instance in the `LocalAgent.Subscription.{i}.` data model of the main USP agent, because it has no knowledge of how the data model is distributed internally. The main agent is then responsible for communicating to the reduced agents that a subscription has been made for a part of the data model they manage. This could be done by duplicating the `Subscription.{i}.` data model under each of the reduced agents. However if there are many reduced agents, this means a lot of duplication of the `Subscription.{i}.` data model. There is also no real reason to have this data model, because there will never be a controller that will access this data model. It shouldn't even be visible for an external controller, because it is not part of the TR-181 data model. An alternative approach would be to add a subscribe/unsubscribe USP message.